/* Import modules and classes. */
import * as storage from './modules/storage';
import Animation from './classes/Animation';


/* Store animations and keep up with the state. */
var animations = [];
var playing = null;


/* Helper functions */

/**
 * Neutralizes the raw input from the user for parsing.
 * @param  {string} rawInput The raw input from the user.
 * @return {string}          Neutralized program.
 */
function _neutralizeProgram(rawInput) {
  /* Replace // -comments */
  var replaced = rawInput.replace(/\/\/.*$/gm, '');
  /* Put everything on one line and remove /* -comments */
  replaced = replaced.replace(/\r?\n|\r/g, '').replace(/\/\*.*\*\//g, '');
  /* Replace all quotes */
  return replaced.replace(/["']|\s/g, '');
}

/**
 * Parses the program for animations.
 * @param  {string} rawInput The raw input from the user.
 * @return {Array}           Parsed array of programs for animations.
 */
function _parseProgram(rawInput) {
  var rawProgram = _neutralizeProgram(rawInput);
  var rawProgramChars = rawProgram.split('');
  var programs = [];

  for (var i = 0; i < rawProgramChars.length; i++) {
    var character = rawProgramChars[i];

    /* Add new animation if command was 'x' or if no animations found */
    if (programs.length === 0 || character === 'x') {
      programs.push([]);
      if (character === 'x') continue;
    }

    /* Start parsing only if command starts from this index */
    if ('esawx'.indexOf(character) > -1) {
      var program = programs[programs.length - 1];
      var commandLength = {a: 1, e: 3, s: 3, w: 5}[character];
      var commandIndex = program.length;

      program.push(character);

      for (var u = 1; u < commandLength; u++) {
        program[commandIndex] += rawProgramChars[i + u];
      }

      i += commandLength - 1;
    }
  }

  return programs;
}

/**
 * Returns either the previous index or the next index so animations
 * never overflows.
 * @param  {int} direction The direction we want to go.
 * @return {int}           The index for the prev/next animation.
 */
function _getIndex(direction) {
  if (direction === 1) {
    if (animations.length - 1 === playing) return 0;
    return playing + 1;
  }
  if (playing === 0) return animations.length - 1;
  return playing - 1;
}

/**
 * Stops the currently playing animation and switches to the next one.
 */
function _nextAnimation() {
  if (playing === null) {
    playing = 0;
  } else {
    animations[playing].stop();
    playing = _getIndex(1);
  }
  animations[playing].start();
}


/* Capturing new program, parsing it and creating new animations. */
$('#new-program').click(function(e) {
  var rawInput = $('#input-code').val();
  var programs = null;

  if (rawInput !== '') {
    programs = _parseProgram(rawInput);

    for (var u = 0; u < animations.length; u++) {
      animations[u].stop();
    }

    animations.length = 0;

    for (var i = 0; i < programs.length; i++) {
      animations.push(new Animation(programs[i]));
    }

    playing = null;
    _nextAnimation();
  }
});


/* Code restoring and saving. */
(function() {
  var savedCode = storage.get('code') || [''];
  if (savedCode[0].length > 0) {
    $('#input-code').val(savedCode);
    $('#new-program').click();
  }
})();

$('#input-code').keyup(function() {
  storage.save('code', [$(this).val()]);
});


/* Controlling the animations. */
$('#next-animation').mousedown(function(e) {
  e.stopPropagation();
  if (playing !== null) animations[playing].stop();
});

$('#next-animation').mouseup(function(e) {
  e.stopPropagation();
  _nextAnimation();
});
