/* Import modules and classes */
import * as storage from './storage';


/* DOM elements for the leds. */
var ledContainer = document.getElementById('matrix');
var leds = ledContainer.children;

/* Selection */
var selected = storage.get('selected') || [];
var brightnesses = {};


/* Populate the leds */
for (var i = 0; i < 16 * 16; i++) {
  ledContainer.appendChild(
      $(document.createElement('div')).addClass('led')[0]
  );
}

_updateMatrix();


/* Helper functions */


/**
 * Returns the CSS color string for a brightness.
 * @param  {int} brightness The brightness to create the string for.
 * @return {string}         The CSS color string.
 */
function _getColorString(brightness) {
  var opacity = brightness / 100;
  return 'background-color: rgba(230, 96, 96,' + opacity + ');';
}

/**
 * Updates a single led in the matrix.
 * @param {int} index The index of the led.
 */
function _updateLed(index) {
  var $led = $(leds[index]);
  var isSelected = selected.indexOf(index) > -1;
  var brightness = brightnesses[index] || 0;

  if ($led.attr('style') && !isSelected) {
    $led.removeClass('-selected');
    $led.removeAttr('style');
  } else if (isSelected) {
    $led.addClass('-selected');
    $led.attr('style', _getColorString(brightness));
  }
}

/** Refreshes the matrix's leds. */
function _updateMatrix() {
  for (var i = 0; i < leds.length; i++) {
    _updateLed(i);
  }
}


/* Public functions */

/**
 * Sets brightness for a single led in the matrix.
 * @param {int} x          The x coordinate of the led.
 * @param {int} y          The y coordinate of the led.
 * @param {int} brightness The brightness to set.
 */
export function set(x, y, brightness) {
  var index = y * 16 + x;
  brightnesses[index] = brightness;
  _updateLed(index);
}

/** Selects every led in the matrix. */
export function selectAll() {
  selected.length = 0;
  for (var i = 0; i < leds.length; i++) {
    selected.push(i);
  }
  storage.save('selected', selected);
  _updateMatrix();
}

/** Clears the current selection. */
export function unselectAll() {
  selected.length = 0;
  storage.save('selected', selected);
  _updateMatrix();
}


/* Selection by clicking a led in the matrix */
$('#matrix').click(function(e) {
  var $target = $(e.target);
  var index = $target.index();

  if ($target.hasClass('led')) {
    e.stopPropagation();
    if (selected.indexOf(index) > -1) {
      selected.splice(selected.indexOf(index), 1);
    } else {
      selected.push(index);
    }
    storage.save('selected', selected);
    _updateMatrix();
  }
});

/* Select all */
$('#select-all').click(function(e) {
  selectAll();
});

/* Unselect all */
$('#unselect-all').click(function(e) {
  unselectAll();
});
