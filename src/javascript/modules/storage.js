var localStorage = window.localStorage;


/**
 * Saves JSON data to the local storage.
 * @param {string} key The key for the data.
 * @param {object} data The data to save.
 */
export function save(key, data) {
  var parsedData = null;
  try {
    parsedData = JSON.stringify(data);
  } catch (e) {
    console.error(e);
    return;
  }
  localStorage.setItem(key, parsedData);
}

/**
 * Gets JSON data from the local storage.
 * @param  {string} key The key for the data.
 * @return {object}     The data object.
 */
export function get(key) {
  var data = localStorage.getItem(key) || null;
  if (data) {
    try {
      data = JSON.parse(data);
    } catch (e) {
      console.error(e);
    }
  }
  return data;
}
