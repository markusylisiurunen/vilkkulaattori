/* Import modules and classes  */
import * as matrix from '../modules/matrix';


/**
 * Constructor for the Led class.
 * @param {int} x The x coordinate of the led.
 * @param {int} y The y coordinate of the led.
 * @constructor
 */
export default function Led(x, y) {
  /* Coordinates */
  this.x = x;
  this.y = y;

  /* Current state */
  this.effect = 0;
  this.lastBrightness = 0;
  this.brightness = 0;
  this.direction = 1;
}

/**
 * Set new effect and brightness for the led.
 * @param {int} effect     The id for the desired effect.
 * @param {int} brightness The brightness for the led (0-100).
 */
Led.prototype.set = function(effect, brightness) {
  this.effect = effect;
  this.brightness = brightness;

  if ([3, 7].indexOf(effect) > -1) {
    this.direction = 1;
  } else {
    this.direction = 1;
  }
};

/**
 * Animate the led's brightness according to the set effect.
 * @param {int} time Time passed since last animation in milliseconds
 */
Led.prototype.next = function(time) {
  var newBrightness = this.brightness;

  if (this.effect === 1) {
    newBrightness = this.brightness - time * 0.15;
    if (newBrightness < 0) newBrightness = 0;
  } else if (this.effect === 3) {
    newBrightness = this.brightness + time * 0.15;
    if (newBrightness > 100) newBrightness = 100;
  } else if (this.effect === 5 || this.effect === 7) {
    if (this.direction === -1 && this.brightness <= 0) this.direction = 1;
    if (this.direction === 1 && this.brightness >= 100) this.direction = -1;
    newBrightness = this.brightness + this.direction * time * 0.22;
  }

  if (newBrightness !== this.lastBrightness) {
    this.brightness = newBrightness;
    this.lastBrightness = this.brightness;
    matrix.set(this.x, this.y, this.brightness);
  }
};
