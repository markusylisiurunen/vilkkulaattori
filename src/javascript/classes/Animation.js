/* Import modules and classes */
import Led from './Led';


/**
 * Constructor for the Animation class.
 * @param {array} commands The array of commands for this animation.
 */
export default function Animation(commands) {
  this.commands = this._groupCommands(commands);
  this.command = 0;

  this.buffer = [];
  this.timeout = null;

  this.leds = {};
  this._populateLeds();

  this.effect = {code: 0, brightness: 0};
  this.isStopped = true;
}

/**
 * Groups all the flat commands into a structure, which can be executed
 * group by group.
 * @param  {Array} commands Flat list of commands.
 * @return {Array}          The grouped array.
 */
Animation.prototype._groupCommands = function(commands) {
  var groups = [];
  var lastCode = null;

  for (var i = 0; i < commands.length; i++) {
    var code = commands[i].split('')[0];

    if (lastCode === null || code !== lastCode) {
      groups.push([commands[i]]);
      lastCode = code;
      continue;
    }

    groups[groups.length - 1].push(commands[i]);
    lastCode = code;
  }

  return groups;
};

/**
 * Populates the leds of this animation.
 */
Animation.prototype._populateLeds = function() {
  for (var y = 0; y < 16; y++) {
    this.leds[y] = {};

    for (var x = 0; x < 16; x++) {
      this.leds[y][x] = new Led(x, y);
    }
  }
};

/**
 * Runs a function for each led.
 * @param {Function} func The function to execute for each led.
 */
Animation.prototype._forEachLed = function(func) {
  for (var y in this.leds) {
    if (this.leds.hasOwnProperty(y)) {
      for (var x in this.leds[y]) {
        if (this.leds[y].hasOwnProperty(x)) {
          func.call(this.leds[y][x]);
        }
      }
    }
  }
};

/**
 * Starts the animation loop for the leds.
 */
Animation.prototype._startAnimationLoop = function() {
  var _this = this;
  var last = Date.now();

  /** Callback for the animation loop. */
  function animationCallback() {
    var elapsed = Date.now() - last;

    while (_this.buffer.length > 0) {
      _this.buffer.shift().call(_this);
    }

    if (_this.isStopped) return;

    _this._forEachLed(function() {
      this.next(elapsed);
    });

    last = Date.now();
    window.requestAnimationFrame(animationCallback);
  }

  window.requestAnimationFrame(animationCallback);
};

/**
 * Executes the next command after some time.
 * @param  {int} time The time to spend before calling the this._next.
 */
Animation.prototype._nextIn = function(time) {
  var _this = this;
  this.timeout = setTimeout(function() {
    _this._nextCommand();
  }, time);
};

/**
 * Executes the next command in the command sequence.
 */
Animation.prototype._nextCommand = function() {
  var group = this.commands[this.command];

  if (this.isStopped) return;

  this.buffer.push(function() {
    var command = null;
    var code = null;
    var time = 0;
    var stop = false;

    for (var i = 0; i < group.length; i++) {
      command = group[i];
      code = command.split('')[0];

      switch (code) {
        case 'e':
          var effect = parseInt(command.split('')[1], 16);
          var brightness = parseInt(command.split('')[2], 16);
          this.effect.code = effect;
          this.effect.brightness = brightness / 15 * 100;
          break;
        case 's':
          var x = parseInt(command.split('')[2], 16);
          var y = parseInt(command.split('')[1], 16);
          this.leds[y][x].set(this.effect.code, this.effect.brightness);
          break;
        case 'a':
          var _this = this;
          this._forEachLed(function() {
            this.set(_this.effect.code, _this.effect.brightness);
          });
          break;
        case 'w':
          if (command.slice(1) === 'ffff') stop = true;
          time = parseInt(command.slice(1), 16) * 15;
          break;
        default:
          break;
      }
    }

    if (stop) return;
    this._nextIn(time);
  });

  if (this.command + 1 === this.commands.length) {
    this.command = 0;
  } else {
    this.command++;
  }
};

/**
 * Start the animation.
 */
Animation.prototype.start = function() {
  this.isStopped = false;
  this._startAnimationLoop();
  this.command = 0;
  this._nextCommand();
};

/**
 * Stop the animation.
 */
Animation.prototype.stop = function() {
  this.isStopped = true;
  this.buffer.length = 0;
  clearTimeout(this.timeout);
  this._forEachLed(function() {
    this.set(0, 0);
    this.next(20);
  });
};
