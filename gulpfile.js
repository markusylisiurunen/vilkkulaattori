var autoprefixer = require('gulp-autoprefixer');
var babelify = require('babelify');
var browserify = require('gulp-browserify');
var concat = require('gulp-concat');
var gulp = require('gulp');
var gulpif = require('gulp-if');
var htmlmin = require('gulp-htmlmin');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var server = require('browser-sync').create();
var uglify = require('gulp-uglify');


gulp.task('html', function() {
  return gulp.src('./src/*.html')
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./public'));
});

gulp.task('sass', function() {
  return gulp.src([
    './bower_components/normalize-css/normalize.css',
    './src/sass/main.scss'
  ])
    .pipe(concat('main.scss'))
    .pipe(sass({
      outputStyle: 'compressed',
      includePaths: ['./src/sass/']
    }).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('./public'));
});

gulp.task('js', function() {
  return gulp.src('./src/javascript/main.js')
    .pipe(browserify({
      transform: babelify
    }))
    .pipe(uglify())
    .pipe(gulp.dest('./public'));
});

gulp.task('libraries', function() {
  return gulp.src([
    './bower_components/jquery/dist/jquery.js'
  ])
    .pipe(gulpif('*.js', uglify()))
    .pipe(gulp.dest('./public/libraries'));
});

gulp.task('watch', gulp.series(
  gulp.parallel('html', 'sass', 'js', 'libraries'), function(done) {
    gulp.watch('./src/**/*', gulp.parallel('html', 'sass', 'js'));
    done();
  }
));

gulp.task('serve', gulp.series('watch', function(done) {
  server.init({
    files: './public',
    server: './public'
  }, done);
}));

gulp.task('default', gulp.parallel('html', 'sass', 'js', 'libraries'));
