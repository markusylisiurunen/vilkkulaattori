# Vilkkulaattori

Projektin tarkoitus on mahdollistaa ledivilkuttimen ohjelmakoodin testaus.
Aiemmin opiskelijoilla ei ollut mahdollisuutta testata ledivilkuttimen
ohjelmakoodia ennen varsinaisen ohjelman asentamista mikrokontrollerille,
jonka takia virheiden korjaaminen lyhyessä ajassa laboratoriossa oli vaikeaa.
Lopputulosta pääset tarkastelemaan [täältä](https://markusylisiurunen.gitlab.io/vilkkulaattori).

## Komennot

Komento | Selitys
--------|------------------------------------
eNN     | Valitse käytettävä efekti
sNN     | Aseta efekti ledille
a       | Aseta efekti kaikille ledeille
wNNNN   | Odota tietty aika
x       | Seuraava animaatiomääritys

## Esimerkkiohjelmia

```javascript
/*
 * Vilkuta kaikkia ledejä.
 */

"e70"
"a"
"wffff"
```

```javascript
/*
 * Vilkuta 8. rivin ja 4. sarakkeen lediä.
 */

"e0f"
"s73"
"w0040"
"e00"
"s73"
"w0040"
```

```javascript
/*
 * Määrittele kaksi eri animaatiota.
 */

 "e70"
 "sc0"
 "e78"
 "sd1"
 "wffff"
 "x"
 "e70"
 "a"
 "wffff"
```
